Summary
-------
This project allows a hypemachine user to download the mp3s of their (or anybody elses) "loved tracks."
The script is fully-functional on Windows systems, though I always intended to clean-up the code after I initially coded it, there isn't really a good reason to at this point since it does what I need it to do.

This software is provided as a proof-of-concept and comes with no guarantee.